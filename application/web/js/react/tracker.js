var RecordPage = React.createClass({

    getInitialState: function() {
        return {
            status: 'closed',
        }
    },

    render: function render() {
        var results = null;
        $.get(
            '/api/1.0/records',
            function(data) {
                results = data;
            }
        );

        console.log(results);

        return (
            <button onClick={this.handleClick}>
                {this.state.status == 'opened' ? 'Open popup' : 'Insert Record'}
            </button>
        )
    },

    handleClick: function (e) {
        this.setState({
            status: 'opened'
        });
    }
});

ReactDOM.render(<RecordPage />, document.getElementById('react-container'));
