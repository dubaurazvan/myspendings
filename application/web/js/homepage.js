// Apply the theme
Highcharts.setOptions(Highcharts.theme);

$(document).ready(function () {
    $.getJSON(apiURL + 'per-day-reports', function (data) {
        var days = [];
        var amounts = [];

        for (i = 0; i < data.length; i++) {
            var date = createUTCdateFromString(data[i].day);
            
            days.push(date.getDate() + ' ' + months[date.getMonth()]);
            amounts.push(Number(data[i].amount));
        }
        
        buildHomepageChart(days, amounts);
    });
});

function buildHomepageChart(categories, data) {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Expenses'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: 'Amount (RON)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: data.length + ' day(s)',
            data: data
        }]
    });
}