var currentPage = 1;

var pageSettings = {
    currentPage: 1,
    user: null,
    filters: {
        categoryId: 0
    }
};


function getListUrl() {
    var url;
    url = apiURL + 'records?page=' + pageSettings.currentPage;

    if (pageSettings.filters.categoryId !== 0) {
        url += '&categoryId=' + pageSettings.filters.categoryId;
    }

    return url;
}

function addRecordsToPage() {
    $.getJSON(getListUrl(), function(data) {
        for (var i = 0; i < data.records.length; i++) {
            var itemHTML = generateRecordHtml(data.records[i]);
            $("#trackerList").append(itemHTML);
        }

        if (data.pagination.prevPage === false) {
            $('.pager .prev-page').addClass('disabled');
        } else {
            $('.pager .prev-page').removeClass('disabled');
        }

        if (data.pagination.nextPage === false) {
            $('.pager .next-page').addClass('disabled');
        } else {
            $('.pager .next-page').removeClass('disabled');
        }
    });
}

/**
 * Get categories from cookie or API
 */
function getCategories() {
    // Get categories list and store it into a cookie and into pageSettings
    var categories = getJsonCookie('categories');
    
    if (categories !== false) {
        addCategories(categories);
    } else {
        $.getJSON(apiURL + 'categories', function(data) {
            addCategories(data);
            setJsonCookie('categories', data, 2);
        });
    }
}

function addCategories(data) {
    
    var shoppingCategories = "";
    for (var i = 0; i < data.categories.length; i++) {
        shoppingNumber = data.categories[i].id;
        shoppingText = data.categories[i].name;
        shoppingCategories += '<option value="' + shoppingNumber + '">' + shoppingText + '</option>';
    }

    $('select[name="category"]').append(shoppingCategories);
    $('select[name="category"] option[value="1"]').prop('selected', 'selected');
    $('select[name="edit-category"]').append(shoppingCategories);
    $('select[name="filter-category"]').append(shoppingCategories);
    
}

function addQuickReports() {
    $.getJSON(apiURL + 'quick-reports', function(data) {
        $("#quickReports-today").text(data.today);
        $("#quickReports-yesterday").text(data.yesterday);
        $("#quickReports-thisMonth").text(data.thisMonth);
        $("#quickReports-lastMonth").text(data.lastMonth);
    });
}

addQuickReports();
getCategories();

function generateRecordHtml(item) {

    var date = createUTCdateFromString(item.createdAt);

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var hoursTime = corectTime(hours);
    var minutesTime = corectTime(minutes);
    
    var html = 
    '<div class="row tracker-item" data-id="' + item.id + '"> \
        <div class="col-xs-10 col-sm-11"> \
            <div class="row"> \
                <div class="col-sm-6 col-xs-12"> \
                    <div class="row">\
                        <span class="trackerName col-sm-8 col-xs-7">' + item.name + '</span> \
                        <span class="col-sm-4 col-xs-5 text-right"> \
                            <span class="trackerAmount">' + item.amount + '</span>&nbsp; \
                            <span class="record-currency">' + item.currency + '</span> \
                        </span> \
                    </div> \
                </div> \
                <div class="col-sm-6 col-xs-12"> \
                    <div class="row">\
                        <span class="trackerCategory col-xs-6 record-category">' + item.category.name + '</span> \
                        <span class="col-xs-6 record-date">' + hoursTime + ':' + minutesTime + ' - ' + months[date.getMonth()] + ' ' + date.getDate() + '</span> \
                    </div> \
                </div> \
            </div> \
        </div> \
        <div class="col-xs-2 col-sm-1"><a href="" class="delete glyphicon glyphicon-remove text-danger" title="Delete Record"></a> <a href="" class="edit glyphicon glyphicon-edit"></a></div> \
    </div>';

    return html;
}

/**
 * Executed when user data is retrived from cookie or API
 */
function userDataAction(userData) {
    pageSettings.user = userData;
    
    // Fill the currency on the entire page
    $('.currency-holder').text(pageSettings.user.currency);
}

$(document).ready(function() {

    addRecordsToPage();

    // Get User information and store it into a cookie and into pageSettings
    var userData = getJsonCookie('user');
    
    if (userData !== false) {
        userDataAction(userData);
    } else {
        $.getJSON(apiURL + 'user', function(data) {
            userDataAction(data);
            setJsonCookie('user', data, 7);
        });
    }
    
    /**
     * Event: Go to next page
     */
    $('.next-page').on('click', function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        pageSettings.currentPage++;
        $("#trackerList").html('');

        addRecordsToPage();
    });

    /**
     * Event: Go to previous page
     */
    $('.prev-page').on('click', function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        pageSettings.currentPage--;
        $("#trackerList").html('');

        addRecordsToPage();
    });

    /**
     * Event: Filter by category
     */
    $('select[name="filter-category"]').on('change', function() {
        pageSettings.filters.categoryId = Number($(this).val());

        $("#trackerList").html('');

        addRecordsToPage();
    });

    /**
     * Bootstrap Event: Close delete record modal
     */
    $('#deleteModal').on('hidden.bs.modal', function() {
        $('.recordDelete').off('click');
    });
    
    /**
     * Bootstrap Event: Open add new record modal
     */
    $("#addRecordModal").on('shown.bs.modal', function() {
        $('input[type="text"]').val("");
        $('select[name="category"] option[value="1"]').prop('selected', 'selected');
    });
    
    /**
     * Event: Open the edit modal and populate the inputs with current record information
     */
    $(document).on('click', '.edit', function() {

        $("#editRecordModal").modal('show');

        var trackerItem = $(this).parents(".tracker-item");

        var necId = trackerItem.data('id');
        var necName = trackerItem.find(".trackerName").html();
        var necPrice = trackerItem.find(".trackerAmount").html();
        var necCategory = trackerItem.find(".trackerCategory").html();

        $("input[name='edit-id']").val(necId);
        $("input[name='edit-name']").val(necName);
        $("input[name='edit-amount']").val(necPrice);
        $('select[name="edit-category"] option:contains("' + necCategory + '")').prop('selected', 'selected');
        
        return false;
    });

    /**
     * Edit record
     */
    $(document).on('click', '.saveNew', function() {

        var id = $("input[name='edit-id']").val();

        var item = {
            name: $("input[name='edit-name']").val(),
            amount: $("input[name='edit-amount']").val(),
            currency: pageSettings.user.currency,
            category: {
                id: $('select[name="edit-category"]').val()
            }
        };

        /**
         * Send request to server to edit the current record
         */
        $.ajax({
            method: "PUT",
            url: apiURL + 'records/' + id,
            contentType: 'application/json',
            data: JSON.stringify(item)
        }).success(function(data) {

            var trackerItem = $('.tracker-item[data-id="' + id + '"]');

            trackerItem.find(".trackerName").text(item.name);
            trackerItem.find(".trackerAmount").text(item.amount);
            trackerItem.find(".trackerCategory").text(
                    $('select[name="edit-category"] option:selected').text()
                    );
            $("#editRecordModal").modal('hide');

            addQuickReports();
        }).error(function(data) {
        });

        return false;
    });

    /**
     * Add new Record
     */
    $("#trackerSubmit").on('click', function() {

        var item = {
            name: $("input[name='name']").val(),
            amount: $("input[name='amount']").val(),
            currency: pageSettings.user.currency,
            category: {
                id: $('select[name="category"]').val()
            }
        };

        /**
         * Send request to server to add a new record
         */
        $.ajax({
            method: "POST",
            url: apiURL + 'records',
            data: JSON.stringify(item),
            contentType: 'application/json'
        }).success(function(data) {
            var itemHTML = generateRecordHtml(data);
            $("#trackerList").prepend(itemHTML);

            $("#addRecordModal").modal('hide');

            addQuickReports();
        }).error(function(data) {
            var alertDangerText = "";
            for (var i = 0; i < data.responseJSON.errors.length; i++) {
                alertDangerText += (i > 0 ? '<br />' : '') + data.responseJSON.errors[i];
            }
            $(".textDanger").html(alertDangerText);
            $(".textDanger").show();
        });

        return false;
    });
    
    /**
     * Add a Record
     */
    $(document).on('click', '.delete', function() {
        var id = $(this).parents(".tracker-item").data('id');
        var trackerItem = $(this).parents(".tracker-item");
        $('#deleteModal').modal('show');

        /**
         * Send request to server to delete a record
         */
        $(".recordDelete").on("click", function() {
            $.ajax({
                type: "DELETE",
                url: apiURL + 'records/' + id
            }).success(function(data) {
                trackerItem.remove();
            }).error(function(data) {
                // Do something
            });
        });

        return false;
    });
});