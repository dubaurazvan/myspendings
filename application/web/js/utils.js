var apiURL = '/api/1.0/';

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function setJsonCookie(name, value, exdays) {
    var d, expires;
    
    d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    expires = "expires=" + d.toUTCString();
    
    console.log(value);
    
    console.log(name + "=" + JSON.stringify(value) + "; " + expires);
    document.cookie = name + "=" + JSON.stringify(value) + "; " + expires;
}

function getJsonCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return JSON.parse(c.substring(name.length, c.length));
    }
    return false;
}

function corectTime(i) {
    if (i < 10) {
        return "0" + i;
    } else {
        return i;
    }
}

function createUTCdateFromString(dateAsString) {
    // Fix for safari
    var a = dateAsString.split(/[^0-9]/);
    var date = new Date();
    date.setUTCFullYear(a[0]);
    date.setUTCMonth(a[1]-1);
    date.setUTCDate(a[2]);
    date.setUTCHours(a[3]);
    date.setUTCMinutes(a[4]);
    date.setUTCSeconds(a[5]);
    
    return date;
}