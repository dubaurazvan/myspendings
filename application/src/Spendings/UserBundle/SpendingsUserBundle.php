<?php

namespace Spendings\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SpendingsUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
