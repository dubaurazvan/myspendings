<?php

namespace Spendings\UserBundle\Service;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User management Service
 * 
 * Provides the user actions for create and update.
 *
 * @package      Spendings\UserBundle\Service
 * @author       Dubau Razvan <dubau_razvan@yahoo.com>
 */
class UserManager
{
    /**
    * Store the given information to Database
    *
    * @param   UserInterface $user
    * @return  void
    */
    public function storeUser(UserInterface $user)
    {
        if (!$user->getId()) {
            $password = $this->encoderFactory->getEncoder($user)
                ->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);
        }
        
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
    
    /**
    * Remove User entity from Database
    *
    * @param   UserInterface $user
    * @return  void
    */
    public function removeUser(UserInterface $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
    
}