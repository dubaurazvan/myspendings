<?php

namespace Spendings\UserBundle\Service;

use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Role management Service
 * 
 * Provides the user actions for create and update.
 *
 * @package      Spendings\UserBundle\Service
 * @author       Dubau Razvan <dubau_razvan@yahoo.com>
 */
class RoleManager
{
    /**
    * Store the given information to Database
    *
    * @param   RoleInterface $role
    * @return  void
    */
    public function storeRole(RoleInterface $role)
    {
        $tempRole = $this->entityManager
                         ->getRepository('SpendingsUserBundle:Role')
                         ->findOneBy(array('role' => $role->getRole()));
        
        if (!$tempRole) {
            $this->entityManager->persist($role);
        }
        
        return $role;
    }
    
    /**
     * EntityManager flush
     * 
     * @return  void
     */
    public function flush()
    {
        $this->entityManager->flush();
    }
    
    /**
     * Get the ROLE_ADMIN Database object
     * 
     * @return RoleInterface
     */
    public function getAdminRole()
    {
        return $this->entityManager
                    ->getRepository('SpendingsUserBundle:Role')
                    ->findOneBy(array('role' => 'ROLE_ADMIN'));
    }
}