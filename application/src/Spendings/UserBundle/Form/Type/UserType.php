<?php

namespace Spendings\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * User Type
 * 
 * Provides the user register/edit form.
 *
 * @package      Spendings\UserBundle\Service
 * @author       Dubau Razvan <razvan.dubau@evozon.com>
 * @copyright    Copyright (c) 2014 Evozon Systems (http://www.evozon.com/)
 */
class UserType extends AbstractType
{
    /**
    * Build register/edit Form
    *
    * @param   Symfony\Component\Form\FormBuilderInterface $builder
    * @return  void
    */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('roles', 'entity', array(
            'class' => 'SpendingsUserBundle:Role',
            'property' => 'role',
            'multiple' => true
        ));
        $builder->add('first_name', 'text');
        $builder->add('last_name', 'text');
        $builder->add('email', 'email');
        $builder->add('username', 'text');
        if (!isset($options['data'])) {
            $builder->add('password', 'password');    
        }
    }

    /**
    * Set data provider for the current Form
    *
    * @param   Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
    * @return  void
    */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Spendings\UserBundle\Entity\User',
            'validation_groups' => function(FormInterface $form) {
                $data = $form->getData();
                if ($data->getId()) {
                    return array('Default');
                }
                else {
                    return array('Default', 'register');
                }
            },
        ));
        
    }
    
    /**
     * Returns the name of this type.
     * 
     * Abstact implementation
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user';
    }
}
