<?php

namespace Spendings\UserBundle\Handler;
 
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\SecurityContext;
 
/**
 * Login Handler
 * 
 * Handle the user access and redirect the new authenticated user to it's current index
 *
 * @package      Spendings\UserBundle\Handler
 * @author       Dubau Razvan <razvan.dubau@evozon.com>
 * @copyright    Copyright (c) 2014 Evozon Systems (http://www.evozon.com/)
 */
class LoginHandler implements AuthenticationSuccessHandlerInterface
{
    
    /**
    * Handle the successfully authentication
    *
    * @param   Request $request
    * @param   TokenInterface $token
    * @return  RedirectResponse
    */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $response = new RedirectResponse($this->router->generate('user_users'));
        }
        elseif ($this->security->isGranted('ROLE_USER')) {
            $response = new RedirectResponse($this->router->generate('spendings_app_homepage'));
        }
        else {
            $response = new RedirectResponse($this->router->generate('spendings_app_homepage'));
        }

        return $response;
    }
    
}