<?php

namespace Spendings\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request as Request;

class WelcomeController extends Controller
{
    public function userAction(Request $request)
    {
        return $this->render('SpendingsUserBundle:Welcome:user.html.twig');
    }
}
