<?php

namespace Spendings\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Spendings\UserBundle\Form\Type\UserType;

class UsersController extends Controller
{
    public function listAction($_route)
    {
        $users = $this->getDoctrine()
            ->getRepository('SpendingsUserBundle:User')
            ->findAll();
        
        return $this->render('SpendingsUserBundle:User:list.html.twig', array('users' => $users));
    }

    public function editAction($id, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('SpendingsUserBundle:User')
            ->find($id);
        
        $form = $this->createForm(new UserType(),  $user);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $this->get('user_manager')->storeUser($form->getData());

            return $this->redirect($this->generateUrl('spendings_user_list'));
        }
        
        return $this->render('SpendingsUserBundle:User:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function addAction(Request $request)
    {
        $form = $this->createForm(new UserType());

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $this->get('user_manager')->storeUser($form->getData());

            return $this->redirect($this->generateUrl('spendings_user_list'));
        }
        
        return $this->render('SpendingsUserBundle:User:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function deleteAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('SpendingsUserBundle:User')
            ->find($id);
        
        $this->get('user_manager')->removeUser($user);
        
        return $this->redirect($this->generateUrl('user_users'));
    }
}