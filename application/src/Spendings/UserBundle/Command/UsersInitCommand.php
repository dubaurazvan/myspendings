<?php

namespace Spendings\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Spendings\UserBundle\Entity\User;
use Spendings\UserBundle\Entity\Role;

class UsersInitCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('users:init')
            ->setDescription('Creates the default admin user and default roles')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->addDefaultRoles();
        
        // Get ROLE_ADMIN object
        $role = $this->roleManager->getAdminRole();
        
        $user = new User();
        $user->setPassword(base64_decode('cm9iZXJ0bzg4'));
        $user->setUsername('admin');
        $user->setFirstName('Admin');
        $user->setLastName('Admin');
        $user->setEmail('contact@extragsm.com');
        $user->addRole($role);
        
        try {
            $this->userManager->storeUser($user);
            $output->writeln("Admin added succesfully");
        } catch (\Doctrine\DBAL\DBALException $e) {
            $output->writeln("<error>Admin was already added</error>");
        }
    }
    
    public function addDefaultRoles()
    {
        // ROLE_ADMIN
        $roleAdmin = new Role();
        $roleAdmin->setName('Admin');
        $roleAdmin->setRole('ROLE_ADMIN');
        $this->roleManager->storeRole($roleAdmin);
        
        // ROLE_USER
        $roleUser = new Role();
        $roleUser->setName('User');
        $roleUser->setRole('ROLE_USER');
        $this->roleManager->storeRole($roleUser);
        
        $this->roleManager->flush();
    }
}