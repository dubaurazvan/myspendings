<?php

namespace Spendings\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Spendings\UserBundle\Entity\User;
use Spendings\UserBundle\Entity\Role;

class ResendConfirmationEmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fos:users:resend-confirmation')
            ->setDescription('Resend confirmation email to user')
            ->addArgument('email', InputArgument::REQUIRED, 'The user email')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $name = $input->getArgument('email');
        
        $mailer = $this->getContainer()->get('fos_user.mailer');
        
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        
        $user = $em->getRepository('SpendingsUserBundle:User')
            ->findOneBy(array('email' => $email));

        // Create token
        $token = sha1(uniqid(mt_rand(), true)); // Or whatever you prefer to generate a token
        $user->setConfirmationToken($token);

        $em->flush($user);

        $mailer->sendConfirmationEmailMessage($user);
    }
    
}

