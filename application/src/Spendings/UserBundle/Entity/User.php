<?php
namespace Spendings\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\VirtualProperty;
use Swagger\Annotations as SWG;

/**
 * Spendings\UserBundle\Entity\User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * 
 * @ExclusionPolicy("all")
 * @SWG\Definition(definition="User", required={"email","firstName","lastName","password"})
 * 
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=30)
     * 
     * @Expose
     * @SWG\Property(description="The User first name")
     */
    protected $firstName;
    
    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", length=30)
     * 
     * @Expose
     * @SWG\Property(description="The User last name")
     */
    protected $lastName;
    
    /**
     * @var Record Collection 
     * 
     * @ORM\OneToMany(targetEntity="Spendings\ApiBundle\Entity\Record", mappedBy="user")
     */
    protected $records;
    
    /**
     * @var string
     * @ORM\Column(name="country", type="string", length=2)
     * 
     * @Expose
     * @SWG\Property(description="The User country")
     */
    protected $country;
    
    /**
     * @var string
     * @ORM\Column(name="currency", type="string", length=3)
     * 
     * @Expose
     * @SWG\Property(description="The User default currency")
     */
    protected $currency;
    
    /**
     * Construct method
     * 
     * return void
     */
    public function __construct()
    {
        parent::__construct();
        
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
        //$this->roles = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
//    public function getRoles()
//    {
//       // return array('ROLE_USER');
//       return $this->roles->toArray();
//    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    
    /**
     * Set lastLogin dateTime
     *
     * @param DateTime $lastLogin
     * @return User
     */
    public function setLastLogin(\DateTime $lastLogin = null)
    {
        if (!$lastLogin) {
            $lastLogin = new \DateTime();
        }
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastlogin dateTime
     *
     * @return string 
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }
    
    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setDefaultRole()
    {
        $this->addRole('ROLE_USER');
    }
    
    /**
     * Add roles
     *
     * @param integer $roles
     * @return User
     */
//    public function addRole($roles)
//    {
//        $this->roles[] = $roles;
//
//        return $this;
//    }
    
    /**
     * Remove role
     *
     * @param string $role
     */
//    public function removeRole($role)
//    {
//        $this->roles->removeElement($role);
//    }

    /**
     * Add records
     *
     * @param \Spendings\ApiBundle\Entity\Record $records
     * @return User
     */
    public function addRecord(\Spendings\ApiBundle\Entity\Record $records)
    {
        $this->records[] = $records;

        return $this;
    }

    /**
     * Remove records
     *
     * @param \Spendings\ApiBundle\Entity\Record $records
     */
    public function removeRecord(\Spendings\ApiBundle\Entity\Record $records)
    {
        $this->records->removeElement($records);
    }

    /**
     * Get records
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecords()
    {
        return $this->records;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set currency
     *
     * @param string $currency
     * @return User
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
