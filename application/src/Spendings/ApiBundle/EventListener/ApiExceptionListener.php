<?php

namespace Spendings\ApiBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use FOS\RestBundle\Util\Codes;

/**
 * ClassName
 * @package      Spendings\ApiBundle\EventListener
 * @author       Razvan Dubau <dubau_razvan@yahoo.com>
 * @copyright    Copyright (c) 2006 Extragsm (http://www.extragsm.com/)
 */
class ApiExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // do whatever tests you need - in this example I filter by path prefix
        $path = $event->getRequest()->getRequestUri();
        if (!preg_match('/\/api\//', $path)) {
            return;
        }
        
        $exception = $event->getException();

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $message = '';
            switch ($exception->getStatusCode()) {
                case Codes::HTTP_METHOD_NOT_ALLOWED:
                    $message = 'Method not allowed';
                break;
            
                case Codes::HTTP_NOT_FOUND:
                    $message = 'Page not found';
                break;
            
                default:
                    $message = $exception->getMessage();
            }
            
            $event->setResponse(new JsonResponse(array('message' => $message), $exception->getStatusCode()));
            $event->stopPropagation();
        }
    }
}