<?php

namespace Spendings\ApiBundle\Util;

/**
 * ClassName
 * @package      Spendings\ApiBundle\Util
 * @author       Razvan Dubau <dubau_razvan@yahoo.com>
 * @copyright    Copyright (c) 2006 Extragsm (http://www.extragsm.com/)
 */
class RecordTypeTransformer
{
    const DEFAULT_VALUE = 0;
    
    public static function transform($value)
    {
        return isset($value['id']) ? $value['id'] : static::DEFAULT_VALUE;
    }
}
