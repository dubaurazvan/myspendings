<?php

namespace Spendings\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;
use Spendings\ApiBundle\Entity\QuickReport;
use Swagger\Annotations as SWG;

/**
 * RecordRepository (Records object)
 * 
 * @package      Spendings\ApiBundle\Entity
 * @author       Razvan Dubau <dubau_razvan@yahoo.com>
 * @copyright    Copyright (c) 2006 Extragsm (http://www.extragsm.com/)
 * 
 * @SWG\Definition(
 *      definition="Records",
 *      @SWG\Property(
 *          property="records",
 *          description="Array of [Record](#/definitions/Record)",
 *          type="array",
 *          @SWG\Items(ref="#/definitions/Record")
 *      ),
 *      @SWG\Property(
 *          property="pagination",
 *          description="Pagination object",
 *          ref="#/definitions/Pagination"
 *      )
 * )
 */
class RecordRepository extends EntityRepository
{
    /**
     * Get the record list for a user
     * 
     * @todo Add user link to query
     * 
     * @param type $firstResult
     * @param type $maxResults
     * @return Record[]
     */
    public function findAllByUser($firstResult, $maxResults, $user, $filters = array())
    {
        
        $filterSQL = '';
        foreach ($filters as $col => $value) {
            $filterSQL .= ' AND ' . addslashes($col) . ' = \'' . addslashes($value) . '\'';
        }
        
        // Get the records list
        return $this->getEntityManager()
            ->createQuery('
                SELECT r 
                FROM SpendingsApiBundle:Record r
                WHERE r.user = ' . $user->getId() . 
                $filterSQL . '
                ORDER BY r.createdAt DESC
            ')
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults)
            ->getResult();
    }
    
    /**
     * Get the count of all records for current user
     * 
     * @todo Add user link to query
     * 
     * @return integer
     */
    public function countAllByUser($user, $filters = array())
    {
        $filterSQL = '';
        foreach ($filters as $col => $value) {
            $filterSQL .= ' AND ' . addslashes($col) . ' = \'' . addslashes($value) . '\'';
        }
        
        return $this->getEntityManager()
            ->createQuery('SELECT COUNT(r.id) as cnt FROM SpendingsApiBundle:Record r WHERE r.user = ' . $user->getId() . $filterSQL)
            ->getSingleScalarResult();
    }
    
    /**
     * Get quick reports of spendings for a user
     * Quick reports include: today, yesterday, this month and last month
     * 
     * @param type $user
     * @return type
     */
    public function getQuickreportsForUser($user)
    {
        $userId = $user->getId();
        $em = $this->getEntityManager();
        
        // Today
        $todayStart = DateTimeConverter::getDate('today')->format('Y-m-d H:i:s');
        
        // Yesterday
        $yesterdayStart = DateTimeConverter::getDate('yesterday')->format('Y-m-d H:i:s');
        
        // This month
        $thisMonthStart = DateTimeConverter::getDate('first day of this month')->format('Y-m-d H:i:s');
        
        // Last month
        $lastMonthStart = DateTimeConverter::getDate('first day of last month')->format('Y-m-d H:i:s');
        
        $sql = <<<SQL
SELECT 
    'today' as `date`, SUM(r.amount) as `amount`
FROM records r 
WHERE r.user_id = $userId
    AND r.type = 0
    AND r.created_at >= '{$todayStart}'

UNION ALL

SELECT 
    'yesterday' as `date`, SUM(r.amount) as `amount`
FROM records r
WHERE r.user_id = $userId
    AND r.type = 0
    AND r.created_at >= '{$yesterdayStart}'
    AND r.created_at <= '{$todayStart}'
    
UNION ALL

SELECT 
    'thisMonth' as `date`, SUM(r.amount) as `amount`
FROM records r
WHERE r.user_id = $userId
    AND r.type = 0
    AND r.created_at >= '{$thisMonthStart}'
    
UNION ALL

SELECT 
    'lastMonth' as `date`, SUM(r.amount) as `amount`
FROM records r
WHERE r.user_id = $userId
    AND r.type = 0
    AND r.created_at >= '{$lastMonthStart}'
    AND r.created_at <= '{$thisMonthStart}'
SQL;
    
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $report = $stmt->fetchAll();
        
        $quickRep = array();
        foreach ($report as $rep) {
            $quickRep[$rep['date']] = $rep['amount'];
        }
        
        $quickReport = new QuickReport;
        return $quickReport->setToday($quickRep['today'])
                ->setYesterday($quickRep['yesterday'])
                ->setThisMonth($quickRep['thisMonth'])
                ->setLastMonth($quickRep['lastMonth']);
    }
    
    public function getPerDayReports($start, $end, $user)
    {
        $userId = $user->getId();
        $em = $this->getEntityManager();
        
        $start = new \DateTime($start);
        $end = new \DateTime($end);

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');
        
        $sql = <<<SQL
SELECT  
    SUM(amount) as amount, 
    DATE_FORMAT(CONVERT_TZ(created_at,'GMT','Europe/Bucharest'), '%Y-%m-%d 00:00:00') as `day`
FROM records 
WHERE user_id = {$userId}
AND created_at >= '{$start}'
AND created_at <= '{$end}'
GROUP BY `day`
ORDER BY `day` ASC
SQL;

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll();
        
        return $items;
    }
}