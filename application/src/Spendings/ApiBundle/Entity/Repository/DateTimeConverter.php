<?php

namespace Spendings\ApiBundle\Entity\Repository;

/**
 * DateTime Converter for mysql queries
 * Is adapting the desired search dates to server timezone dates
 * 
 * @package      Spendings\AppBundle\Entity\Repository
 * @author       Razvan Dubau <dubau_razvan@yahoo.com>
 * @copyright    Copyright (c) 2006 Extragsm (http://www.extragsm.com/)
 */
class DateTimeConverter
{
    public static function getDate($time, $userTimeZone = 'Europe/Bucharest')
    {
        $serverTimezone = new \DateTimeZone(date_default_timezone_get());
        $gmtTimezone = new\DateTimeZone('GMT');
        
        $date = new \DateTime($time, $serverTimezone);
        
        if (preg_match('/first day of/', $time)) {
            $date->setTime(0,0,0);    
        }
        
        // Correcting the offset for server timezone to GMT
        $serverOffset = $serverTimezone->getOffset($date) - $gmtTimezone->getOffset($date);
        if ($serverOffset < 0) {
            $date->sub(new \DateInterval('PT' . abs($serverOffset) . 'S'));
        } else {
            $date->add(new \DateInterval('PT' . abs($serverOffset) . 'S'));
        }
        
        // Correcting the offset for GMT to user timezone
        $userTimezone = new \DateTimeZone($userTimeZone);
        $userOffset = $gmtTimezone->getOffset($date) - $userTimezone->getOffset($date);
        if ($userOffset < 0) {
            $date->sub(new \DateInterval('PT' . abs($userOffset) . 'S'));
        } else {
            $date->add(new \DateInterval('PT' . abs($userOffset) . 'S'));
        }
        
        return $date;
    }
}