<?php

/**
 * CategoryRepository (Categories object)
 * 
 * @package      Spendings\AppBundle\Entity
 * @author       Razvan Dubau <dubau_razvan@yahoo.com>
 * @copyright    Copyright (c) 2006 Extragsm (http://www.extragsm.com/)
 * 
 * @SWG\Definition(
 *      definition="Categories",
 *      @SWG\Property(
 *          property="categories",
 *          description="Array of [Category](#/definitions/Category)",
 *          type="array",
 *          @SWG\Items(ref="#/definitions/Category")
 *      )
 * )
 */
