<?php

namespace Spendings\ApiBundle\Entity;

use JMS\Serializer\Annotation\Exclude;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * Category
 *
 * @ORM\Table(name="categories")
 * @ORM\Entity
 * 
 * @SWG\Definition(definition="Category", required={"name","amount"})
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @SWG\Property(description="The unique identifier representing a specific record.")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @SWG\Property(description="The name of category.")
     */
    private $name;

    /**
     * @var Record Collection 
     *
     * @Exclude
     * 
     * @ORM\OneToMany(targetEntity="Spendings\ApiBundle\Entity\Record", mappedBy="category")
     */
    private $records;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
