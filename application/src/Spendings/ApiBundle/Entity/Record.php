<?php

namespace Spendings\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Spendings\ApiBundle\ValueObject\RecordType;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Type;
use Spendings\UserBundle\Entity\User;
use Spendings\ApiBundle\Entity\Category;
use Swagger\Annotations as SWG;

/**
 * Record
 *
 * @ORM\Table(name="records")
 * @ORM\Entity(repositoryClass="Spendings\ApiBundle\Entity\Repository\RecordRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ExclusionPolicy("all")
 * 
 * @SWG\Definition(definition="Record", required={"name","amount"})
 */
class Record
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     * @SWG\Property(description="The unique identifier representing a specific record.")
     */
    private $id;

    /**
     * @var RecordType
     *
     * @ORM\Column(name="type", type="string", length=1)
     * 
     * @SWG\Property(description="The type of the record. Default value: 0", ref="#/definitions/RecordType")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @SWG\Property(description="The name of the Record.")
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     * 
     * @SWG\Property(description="The amount of money spent or received.")
     */
    private $amount;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="currency", type="string", length=3)
     *
     * @SWG\Property(description="The currency of money spent or received.")
     */
    private $currency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * 
     * @SWG\Property(description="The date when the record was created.", @SWG\Schema(readOnly=true))
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Spendings\UserBundle\Entity\User", inversedBy="records")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Spendings\ApiBundle\Entity\Category", inversedBy="records")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * 
     * @SWG\Property(description="The category of the record. Default value: 1", ref="#/definitions/Category")
     * 
     * @Expose
     */
    private $category;
    
    /**
     * Construc the Record object
     */
    public function __construct()
    {
        $this->type = isset($this->type) ? 1 : 0;
    }
    
    /**
     * Get id
     *
     * @VirtualProperty
     * 
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Record
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return array 
     * 
     * @VirtualProperty
     * @Type("array")
     */
    public function getType()
    {
        return RecordType::getById($this->type);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Record
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @VirtualProperty
     * 
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Record
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @VirtualProperty
     * 
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set currency
     *
     * @param float $currency
     * @return Record
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @VirtualProperty
     * 
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @VirtualProperty
     * 
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set user
     * 
     * @param User $user
     * @return \Spendings\ApiBundle\Entity\Record
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        
        return $this;
    }
    
    /**
     * Set category
     * 
     * @param Category $category
     * @return \Spendings\ApiBundle\Entity\Record
     */
    public function setCategory($category)
    {
        $this->category = $category;
        
        return $this;
    }
    
    /**
     * Get category
     * 
     * @return \Spendings\ApiBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    public function asArray()
    {
        return array(
            'id' => $this->id,
            'type' => $this->getType(),
            'name' => $this->name,
            'amount' => $this->amount,
            'createdAt' => $this->createdAt,
        );
    }
}
