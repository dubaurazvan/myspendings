<?php

namespace Spendings\ApiBundle\Entity;

use Swagger\Annotations as SWG;

/**
 * Quick reports
 *
 * @SWG\Definition(definition="QuickReport")
 */
class QuickReport
{
    /**
     * @var float
     *
     * @SWG\Property(description="The amount spent today")
     */
    private $today = 0;

    /**
     * @var float
     *
     * @SWG\Property(description="The amount spent yesterday")
     */
    private $yesterday = 0;

    /**
     * @var float
     *
     * @SWG\Property(description="The amount spent this month")
     */
    private $thisMonth;

    /**
     * @var float
     *
     * @SWG\Property(description="The amount spent last month")
     */
    private $lastMonth;

    /**
     * Set the amount spent today
     *
     * @param float $amount
     * @return QuickReport
     */
    public function setToday($amount)
    {
        $this->today =  (float) $amount;

        return $this;
    }
    
    /**
     * Get the amount spent today 
     * 
     * @return integer 
     */
    public function getToday()
    {
        return $this->today;
    }
    
    /**
     * Set the amount spent yesterday
     *
     * @param float $amount
     * @return Record
     */
    public function setYesterday($amount)
    {
        $this->yesterday = (float) $amount;

        return $this;
    }
    
    /**
     * Get the amount spent yesterday 
     * 
     * @return integer 
     */
    public function getYesterday()
    {
        return $this->yesterday;
    }
    
    /**
     * Set the amount spent this month
     *
     * @param float $amount
     * @return QuickReport
     */
    public function setThisMonth($amount)
    {
        $this->thisMonth = (float) $amount;

        return $this;
    }
    
    /**
     * Get the amount spent this month 
     * 
     * @return integer 
     */
    public function getThisMonth()
    {
        return $this->thisMonth;
    }
    
    /**
     * Set the amount spent last month
     *
     * @param float $amount
     * @return Record
     */
    public function setLastMonth($amount)
    {
        $this->lastMonth = (float) $amount;

        return $this;
    }
    
    /**
     * Get the amount spent last month
     * 
     * @return integer 
     */
    public function getLastMonth()
    {
        return $this->lastMonth;
    }
    
}