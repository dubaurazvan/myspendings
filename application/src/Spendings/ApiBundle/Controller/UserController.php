<?php

namespace Spendings\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Spendings\AppBundle\Entity\Record;
use Spendings\AppBundle\Form\RecordType;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\HttpFoundation\RequestStack;
use Swagger\Annotations as SWG;
use Spendings\AppBundle\Entity\QuickReport;


class UserController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Constructor
     * 
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }
    
    /**
     * Get Service
     * 
     * @param string $serviceName
     * @return Symfony\Component\DependencyInjection\Container
     */
    public function get($serviceName)
    {
        return $this->serviceContainer->get($serviceName);
    }
    
    /**
     * @SWG\Get(
     *      path="/user",
     *      summary="Logged in user information",
     *      @SWG\Tag(name="User"),
     *      @SWG\Response(
     *          response="200", 
     *          description="Returns a User object",
     *          @SWG\Schema(ref="#/definitions/User")
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
     */
    public function getAction()
    {
        return $this->get('security.token_storage')->getToken()->getUser();
    }
}
