<?php

namespace Spendings\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Request;
use Spendings\ApiBundle\ValueObject\RecordType;
use Swagger\Annotations as SWG;

class RecordTypeController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @SWG\Get(
     *      path="/record/types",
     *      summary="Get the entire list of record types",
     *      @SWG\Tag(name="RecordTypes"),
     *      @SWG\Response(
     *          response="200", 
     *          description="Returns array of RecordType[]",
     *          @SWG\Schema(ref="#/definitions/RecordTypes")
     *      ),
     *      @SWG\Response(
     *          response="405", 
     *          description="Method not allowed",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
     */
    public function cgetAction()
    {
        return RecordType::getAll();
    }
}
