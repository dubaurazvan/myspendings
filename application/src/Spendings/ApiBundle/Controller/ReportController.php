<?php

namespace Spendings\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\HttpFoundation\RequestStack;
use Spendings\ApiBundle\Entity\Record;
use Spendings\ApiBundle\Form\RecordType;
use Spendings\ApiBundle\Entity\QuickReport;
use Swagger\Annotations as SWG;

class ReportController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @var type Request
     */
    protected $request;
    
    /**
     * Constructor
     * 
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }
    
    /**
     * Get Service
     * 
     * @param string $serviceName
     * @return Symfony\Component\DependencyInjection\Container
     */
    public function get($serviceName)
    {
        return $this->serviceContainer->get($serviceName);
    }
    
    /**
     * @SWG\Get(
     *      path="/quick-reports",
     *      summary="Quick reports",
     *      @SWG\Tag(name="QuickReport"),
     *      @SWG\Response(
     *          response="200", 
     *          description="Returns a QuickReport object",
     *          @SWG\Schema(ref="#/definitions/QuickReport")
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
     */
    public function quickReportsAction()
    {
        return $this->entityManager
            ->getRepository('SpendingsApiBundle:Record')
            ->getQuickreportsForUser(
                $this->get('security.token_storage')->getToken()->getUser()
            );
    }
    
    /**
     * @SWG\Get(
     *      path="/per-day-report",
     *      summary="Per day report",
     *      @SWG\Tag(name="PerDayReport"),
     *      @SWG\Response(
     *          response="200", 
     *          description="Returns a PerDayReport object",
     *          @SWG\Schema(ref="#/definitions/QuickReport")
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
     */
    public function perDayReportsAction()
    {
        return $this->entityManager
            ->getRepository('SpendingsApiBundle:Record')
            ->getPerDayReports(
                $this->request->get('start', 'today -30 days'),
                $this->request->get('end', 'today'),
                $this->get('security.token_storage')->getToken()->getUser()
            );
    }
}
