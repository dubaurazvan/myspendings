<?php

/**
 * @SWG\Swagger(
 *     basePath="/api/1.0",
 *     host="{{hostname}}",
 *     schemes={"http"},
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Spendings",
 *         description="Rest API for Spendings Tracker application. Will ensure the layer between database and application",
 *         termsOfService="",
 *         @SWG\Contact(name="Razvan Dubau"),
 *         @SWG\License(name="ExtraGSM")
 *     ),
 *     @SWG\Definition(
 *         definition="SimpleErrorMessage",
 *         required={"message"},
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="RecordTypes",
 *         @SWG\Property(
 *             property="recordTypes",
 *             description="Array of [RecordType](#/definitions/RecordType)",
 *             type="array",
 *             @SWG\Items(ref="#/definitions/RecordType")
 *         )
 *     )
 * )
 */