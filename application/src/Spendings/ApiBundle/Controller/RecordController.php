<?php

namespace Spendings\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Spendings\ApiBundle\Entity\Record;
use Spendings\ApiBundle\Form\RecordType;
use Symfony\Component\Validator\ConstraintViolation;
use Spendings\ApiBundle\Util\RecordCategoryTransformer;
use Spendings\ApiBundle\Util\RecordTypeTransformer;
use Symfony\Component\HttpFoundation\RequestStack;
use Swagger\Annotations as SWG;


class RecordController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Constructor
     * 
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }
    
    /**
     * Get Service
     * 
     * @param string $serviceName
     * @return Symfony\Component\DependencyInjection\Container
     */
    public function get($serviceName)
    {
        return $this->serviceContainer->get($serviceName);
    }
    
    /**
     * @SWG\Get(
     *      path="/records",
     *      summary="Get a specific list of records",
     *      description="This resource will provide a list of records for a specific user. The list will be reduced to 20 records (by default). Use page and perPage parameters to navigate through all records",
     *      @SWG\Tag(name="Records"),
     *      @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Specify the page of items needed. (Default: 1)",
     *         type="integer"
     *      ),
     *      @SWG\Parameter(
     *         name="perPage",
     *         in="query",
     *         description="Specify how many items will appear on page. (Default: 20)",
     *         type="integer"
     *      ),
     *      @SWG\Parameter(
     *         name="categoryId",
     *         in="query",
     *         description="Filter results by category id",
     *         type="integer"
     *      ),
     *      @SWG\Response(
     *          response="200", 
     *          description="Returns array of Record[]",
     *          @SWG\Schema(ref="#/definitions/Records")
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
     */
    public function cgetAction()
    {
        $filters = array();
        
        $categoryId = $this->request->get('categoryId', false);
        if ($categoryId) {
            $filters['r.category'] = $categoryId;
        }
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $recordRepo = $this->entityManager
            ->getRepository('SpendingsApiBundle:Record');
        
        $pagination = $this->get('spendings_api.pagination');
        $pagination->setTotalCount($recordRepo->CountAllByUser($user, $filters));
        
        $records = $recordRepo->findAllByUser(
            $pagination->getFirstResultIndex(), 
            $pagination->getPerPage(),
            $user,
            $filters
        );
        
        return $this->view(array(
            'records' => $records,
            'pagination' => $pagination
        ));
    }
    
    /**
     * @SWG\Post(
     *      path="/records",
     *      summary="Add new Record",
     *      description="Will create a new record based on Record object structure",
     *      @SWG\Tag(name="Record"),
     *      @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Record object that need to be added",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Record"),
     *      ),
     *      @SWG\Response(
     *          response="201", 
     *          description="A new Record object was created",
     *          @SWG\Schema(ref="#/definitions/Record")
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
    */
    public function postAction()
    {
        $entity = new Record;
        $form = $this->get('form.factory')
            ->create(
                new RecordType, 
                $entity, 
                array('method' => $this->request->getMethod()
            )
        );
        
        $category = $this->request->get('category');
        $this->request->request->set('category', RecordCategoryTransformer::transform($category));
        
        $type = $this->request->get('type');
        $this->request->request->set('type', RecordTypeTransformer::transform($type));
        
        $form->handleRequest($this->request);
        
        if ($form->isValid()) {
            try {
                $entity->setUser(
                    $this->get('security.token_storage')->getToken()->getUser()
                );
                
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
                return $this->view($entity, Codes::HTTP_CREATED);
            } catch (\Exception $e) {
                return $this->view(array('message' => 'Bad request'), Codes::HTTP_BAD_REQUEST);
            }
        } else {
            $formErrors = array();
            foreach ($form->getErrors(true, false) as $error) {
                $formErrors[] = $error->current()->getMessage();
            }
            
            if (count($formErrors)) {
                return $this->view(array('errors' => $formErrors), Codes::HTTP_BAD_REQUEST);
            }
            
            $validator = $this->get('validator');
            $errors = $validator->validate($entity);
            
            if (count($errors)) {
                $entityErrors = array();
                foreach ($errors as $err) {
                    $entityErrors[] = $err->getMessage();
                }
                return $this->view(array('errors' => $entityErrors), Codes::HTTP_BAD_REQUEST);
            }
        }
    }
    
    /**
     * @SWG\Get(
     *      path="/records/{id}",
     *      summary="Get one Record object by Id",
     *      @SWG\Tag(name="Record"),
     *      @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="The id of the record we need",
     *         required=true,
     *         type="integer"
     *      ),
     *      @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Record object that need to be added",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Record"),
     *      ),
     *      @SWG\Response(
     *          response="200", 
     *          description="Returns a journalist",
     *          @SWG\Schema(ref="#/definitions/Record")
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      ),
     *      @SWG\Response(
     *          response="404", 
     *          description="Not found exception",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
     */
    public function getAction($id)
    {
        $item = $this->entityManager
            ->getRepository('SpendingsApiBundle:Record')
            ->find($id);
        
        if (is_null($item)) {
             return $this->view(array('message' => "Record not found: invalid ID"), Codes::HTTP_NOT_FOUND);
        }
        
        return $this->view($item);
    }
    
    /**
     * @SWG\Put(
     *      path="/records/{id}",
     *      summary="Add new Record",
     *      @SWG\Tag(name="Record"),
     *      @SWG\Response(
     *          response="204", 
     *          description="-",
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      ),
     *      @SWG\Response(
     *          response="404", 
     *          description="Not found exception",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
    */
    public function putAction($id)
    {
        $entity = $this->entityManager->getRepository('SpendingsApiBundle:Record')->find($id);
        
        if (is_null($entity)) {
             return $this->view(array('message' => "Record not found: invalid ID"), Codes::HTTP_NOT_FOUND);
        }
        
        $form = $this->get('form.factory')->create(new RecordType, $entity,array('method' => $this->request->getMethod()));
        
        $category = $this->request->get('category');
        $this->request->request->set('category', RecordCategoryTransformer::transform($category));
        
        $type = $this->request->get('type');
        $this->request->request->set('type', RecordTypeTransformer::transform($type));
        
        $form->handleRequest($this->request);
        
        if ($form->isValid()) {
            try {
                $this->entityManager->flush();
                return $this->view('', Codes::HTTP_NO_CONTENT);
            } catch (\Exception $e) {
                return $this->view(array('message' => 'Bad Request' ), Codes::HTTP_BAD_REQUEST);
            }
        } else {
            $formErrors = array();
            foreach ($form->getErrors(true, false) as $error) {
                $formErrors[] = $error->current()->getMessage();
            }
            
            if (count($formErrors)) {
                return $this->view(array('errors' => $formErrors), Codes::HTTP_BAD_REQUEST);
            }
            
            $validator = $this->get('validator');
            $errors = $validator->validate($entity);
            
            if (count($errors)) {
                $entityErrors = array();
                foreach ($errors as $err) {
                    $entityErrors[] = $err->getMessage();
                }
                return $this->view(array('errors' => $entityErrors), Codes::HTTP_BAD_REQUEST);
            }
        }
    }
    
    /**
     * @SWG\Delete(
     *      path="/records",
     *      summary="Remove a Record",
     *      description="Will remove a record from database",
     *      @SWG\Tag(name="Record"),
     *      @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Record id that need to be deleted",
     *         type="integer",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Record")
     *      ),
     *      @SWG\Response(
     *          response="204", 
     *          description="The Record was deleted successfully",
     *          @SWG\Schema(ref="#/definitions/Record")
     *      ),
     *      @SWG\Response(
     *          response="403", 
     *          description="You have to log into application first",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
    */
    public function deleteAction($id)
    {
        $entity = $this->entityManager->getRepository('SpendingsApiBundle:Record')->find($id);
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}
