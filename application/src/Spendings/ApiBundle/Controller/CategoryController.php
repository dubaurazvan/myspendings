<?php

namespace Spendings\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Swagger\Annotations as SWG;

class CategoryController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Constructor
     * 
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }
    
    /**
     * @SWG\Get(
     *      path="/categories",
     *      summary="Get the entire list of categories",
     *      @SWG\Tag(name="Categories"),
     *      @SWG\Response(
     *          response="200", 
     *          description="Returns array of Category[]",
     *          @SWG\Schema(ref="#/definitions/Categories")
     *      ),
     *      @SWG\Response(
     *          response="405", 
     *          description="Method not allowed",
     *          @SWG\Schema(ref="#/definitions/SimpleErrorMessage")
     *      )
     * )
     */
    public function cgetAction()
    {
        $categories = $this->entityManager
            ->getRepository('SpendingsApiBundle:Category')
            ->findBy(array(), array(
                'name' => 'asc'
            ));
        
        
        return array('categories' => $categories);
    }
}
