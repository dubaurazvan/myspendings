<?php

namespace Spendings\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class SwaggerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('swagger:generate:doc')
            ->setDescription('Generate swagger json documention based on annotations added to controllers and models');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $swagger = \Swagger\scan('/var/www/spendings/application/src/Spendings/');
        header('Content-Type: application/json');
        
        file_put_contents('/var/www/spendings/APIDoc/swagger.json', $swagger);
        
        $process = new Process('sh /var/www/spendings/APIDoc/createApiDoc.sh');
        $process->run();
        $output->write($process->getOutput());
                
        $twigFile = '/var/www/spendings/application/src/Spendings/AppBundle/Resources/views/ApiDoc/index.html.twig';
        $docHtml = file_get_contents($twigFile);
        
        $docHtml = str_replace('\n', '<br />', $docHtml);
        file_put_contents($twigFile, $docHtml);
        
        $output->write('<info>Api swagger documentation was updated</info>');
    }
}