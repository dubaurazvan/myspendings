<?php

namespace Spendings\ApiBundle\ValueObject;

use Swagger\Annotations as SWG;

/**
 * RecordType
 * 
 * @SWG\Definition(definition="RecordType", required={"id"})
 */
class RecordType
{
    /**
     * @var integer
     * 
     * @SWG\Property(description="Unique identifier representing a specific record type.")
     */
    private static $id;

    /**
     * @var string
     * 
     * @SWG\Property(description="The name of the RecordType.")
     */
    private static $name;

    private static $all = array(
        array(
            'id' => 0,
            'name' => 'Expense'
        ),
        array(
            'id' => 1,
            'name' => 'Income'
        )
    );
    
    public static function getAll()
    {
        return static::$all;
    }
    
    public static function getById($id)
    {
        static::checkId($id);
        
        return static::$all[$id];
    }
    
    public static function checkId($id)
    {
        if (!isset(static::$all[$id])) {
            throw new \Exception("There is no record type with this id: " . $id);
        }
    }
    
    
}
