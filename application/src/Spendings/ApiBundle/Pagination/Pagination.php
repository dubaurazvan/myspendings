<?php

namespace Spendings\ApiBundle\Pagination;

use Symfony\Component\HttpFoundation\RequestStack;
use Swagger\Annotations as SWG;

/**
 * Pagination
 * 
 * @package      Spendings\ApiBundle\Pagination
 * @author       Razvan Dubau <dubau_razvan@yahoo.com>
 * @copyright    Copyright (c) 2006 Extragsm (http://www.extragsm.com/)
 * 
 * @SWG\Definition(
 *      definition="Pagination",
 * )
 */
class Pagination
{
    /**
     * The selected page
     * 
     * @var integer
     * 
     * @SWG\Property(
     *      property="currentPage",
     *      description="The selected page",
     *      type="integer"
     * )
     */
    protected $currentPage;
    
    /**
     * How many records per page
     * 
     * @var integer
     * 
     * @SWG\Property(
     *      property="perPage",
     *      description="How many records per page",
     *      type="integer"
     * )
     */
    protected $perPage;
    
    /**
     * How many records are in Database
     *
     * @var integer
     * 
     * @SWG\Property(
     *      property="totalCount",
     *      description="How many records are in Database",
     *      type="integer"
     * )
     */
    protected $totalCount;
    
    /**
     * A boolean showing if there is any previous page availavble for current Pagination
     *
     * @var boolean
     * 
     * @SWG\Property(
     *      property="prevPage",
     *      description="A boolean showing if there is any previous page availavble for current Pagination",
     *      type="boolean"
     * )
     */
    protected $prevPage;
    
    /**
     * A boolean showing if there is any next page availavble for current Pagination
     *
     * @var boolean
     * 
     * @SWG\Property(
     *      property="nextPage",
     *      description="A boolean showing if there is any next page availavble for current Pagination",
     *      type="boolean"
     * )
     */
    protected $nextPage;
    
    /**
     * The last available page for current Pagination
     *
     * @var integer
     * 
     * @SWG\Property(
     *      property="lastPage",
     *      description="The last available page for current Pagination",
     *      type="integer"
     * )
     */
    protected $lastPage;
    
    /**
     * Construct
     * 
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();
        
        $this->perPage = $request->get('perPage', 20);
        $this->currentPage = $request->get('page', 1);
        $this->prevPage = $this->currentPage > 1 ? true : false;
    }
    
    /**
     * Set total count
     * 
     * @param integer $count
     * @return \Spendings\ApiBundle\Pagination\Pagination
     */
    public function setTotalCount($count)
    {
        $this->totalCount = $count;
        
        $this->calculateLastPage();
        $this->calculateNextPage();
        
        return $this;
    }
    
    /**
     * Get total count
     * 
     * @return integer
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }
    
    /**
     * Get per page
     * 
     * @return integer
     */
    public function getPerPage()
    {
        return $this->perPage;
    }
    
    /**
     * Get first result index on the current page. 
     * First item will be represented by index = 0
     * 
     * E.g.: 
     * $this->currentPage = 12;
     * $this->perPage = 20 
     * the first result index will be 220
     * 
     * @return integer
     */
    public function getFirstResultIndex()
    {
        return $this->currentPage == 1 ? 0 : (($this->currentPage - 1) * $this->perPage);
    }
    
    /**
     * Calculate the last available page for current Pagination
     * 
     * @throws Exception
     */
    public function calculateLastPage()
    {
        if (is_null($this->totalCount)) {
            throw new Exception("Total count was not set for Pagination");
        }
        $this->lastPage = ceil($this->totalCount / $this->perPage);
    }
    
    /**
     * Calculate the next available page for current Pagination
     * 
     * @throws Exception
     */
    public function calculateNextPage()
    {
        $this->nextPage = $this->currentPage < $this->lastPage ? true : false;
    }
    
    /**
     * Get the last available page for current Pagination
     * 
     * @return integer
     */
    public function getLastPage()
    {
        return $this->lastPage;
    }
    
}