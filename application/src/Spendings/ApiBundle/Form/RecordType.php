<?php

namespace Spendings\ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RecordType extends AbstractType
{
    public function getName()
    {
        return '';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type')
            ->add('name')
            ->add('currency', 'currency', array(
                'required' => true,
                "invalid_message" => "The value \"{{ value }}\" is not a valid {{ type }}.",
                "invalid_message_parameters" => array(
                    "{{ type }}" => "currency"
                )
            ))
            ->add('category', 'entity', array(
                'class' => 'SpendingsApiBundle:Category',
                'data' => 1 // "Default" category id is 1
            ))
            ->add('amount', 'number', array(
                'required' => true,
                "invalid_message" => "The value \"{{ value }}\" is not a valid {{ type }}.",
                "invalid_message_parameters" => array(
                    "{{ type }}" => "amount"
                )
            ))
            ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Spendings\ApiBundle\Entity\Record',
            'allow_extra_fields' => true
        ));
        
    }

}