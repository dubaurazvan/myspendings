<?php

namespace Spendings\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApiDocController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render(
            'SpendingsAppBundle:ApiDoc:index.html.twig',
            array(
                // last username entered by the user
                'hostname' => $request->getHost() . $request->getBaseUrl(),
            )
        );
    }
}
