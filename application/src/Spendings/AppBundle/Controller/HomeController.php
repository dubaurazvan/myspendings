<?php

namespace Spendings\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = $request->getSession();

        if ($this->isGranted('ROLE_USER')) {
            return $this->render(
                'SpendingsAppBundle:Home:index-logged.html.twig',
                array(
                    'user' => $this->getUser()
                )
            );
	    }
        
        return $this->render(
            'SpendingsAppBundle:Home:index.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            )
        );
    }
}
