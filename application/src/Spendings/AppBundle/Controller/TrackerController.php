<?php

namespace Spendings\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

class TrackerController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render(
            'SpendingsAppBundle:Tracker:index.html.twig'
        );
    }
}
