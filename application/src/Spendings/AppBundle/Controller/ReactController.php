<?php

namespace Spendings\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReactController extends Controller
{
    public function indexAction()
    {
        return $this->render(
            'SpendingsAppBundle:React:index.html.twig'

        );
    }
}
