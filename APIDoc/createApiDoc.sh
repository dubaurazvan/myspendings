#!/bin/sh

projectPath="/var/www/spendings"
targetPath=$projectPath"/application/src/Spendings/AppBundle/Resources/views/ApiDoc"

sudo java -jar "$projectPath"/APIDoc/swagger-codegen/modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate \
-i "$projectPath"/APIDoc/swagger.json \
-l html \
-o $targetPath \
-t "$projectPath"/APIDoc/mustache-templates/
echo "Done with generation"

sudo mv "$targetPath"/index.html "$targetPath"/index.html.twig
