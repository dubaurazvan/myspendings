{
    "swagger": "2.0",
    "info": {
        "title": "Spendings",
        "description": "Rest API for Spendings Tracker application. Will ensure the layer between database and application",
        "termsOfService": "",
        "contact": {
            "name": "Razvan Dubau"
        },
        "license": {
            "name": "ExtraGSM"
        },
        "version": "1.0.0"
    },
    "host": "{{hostname}}",
    "basePath": "/api/1.0",
    "schemes": [
        "http"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "paths": {
        "/record/types": {
            "get": {
                "summary": "Get the entire list of record types",
                "responses": {
                    "200": {
                        "description": "Returns array of RecordType[]",
                        "schema": {
                            "$ref": "#/definitions/RecordTypes"
                        }
                    },
                    "405": {
                        "description": "Method not allowed",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            }
        },
        "/quick-reports": {
            "get": {
                "summary": "Quick reports",
                "responses": {
                    "200": {
                        "description": "Returns a QuickReport object",
                        "schema": {
                            "$ref": "#/definitions/QuickReport"
                        }
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            }
        },
        "/per-day-report": {
            "get": {
                "summary": "Per day report",
                "responses": {
                    "200": {
                        "description": "Returns a PerDayReport object",
                        "schema": {
                            "$ref": "#/definitions/QuickReport"
                        }
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            }
        },
        "/user": {
            "get": {
                "summary": "Logged in user information",
                "responses": {
                    "200": {
                        "description": "Returns a User object",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            }
        },
        "/categories": {
            "get": {
                "summary": "Get the entire list of categories",
                "responses": {
                    "200": {
                        "description": "Returns array of Category[]",
                        "schema": {
                            "$ref": "#/definitions/Categories"
                        }
                    },
                    "405": {
                        "description": "Method not allowed",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            }
        },
        "/records": {
            "get": {
                "summary": "Get a specific list of records",
                "description": "This resource will provide a list of records for a specific user. The list will be reduced to 20 records (by default). Use page and perPage parameters to navigate through all records",
                "parameters": [
                    {
                        "name": "page",
                        "in": "query",
                        "description": "Specify the page of items needed. (Default: 1)",
                        "type": "integer"
                    },
                    {
                        "name": "perPage",
                        "in": "query",
                        "description": "Specify how many items will appear on page. (Default: 20)",
                        "type": "integer"
                    },
                    {
                        "name": "categoryId",
                        "in": "query",
                        "description": "Filter results by category id",
                        "type": "integer"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Returns array of Record[]",
                        "schema": {
                            "$ref": "#/definitions/Records"
                        }
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            },
            "post": {
                "summary": "Add new Record",
                "description": "Will create a new record based on Record object structure",
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "description": "Record object that need to be added",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Record"
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "A new Record object was created",
                        "schema": {
                            "$ref": "#/definitions/Record"
                        }
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            },
            "delete": {
                "summary": "Remove a Record",
                "description": "Will remove a record from database",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "Record id that need to be deleted",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Record"
                        },
                        "type": "integer"
                    }
                ],
                "responses": {
                    "204": {
                        "description": "The Record was deleted successfully",
                        "schema": {
                            "$ref": "#/definitions/Record"
                        }
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            }
        },
        "/records/{id}": {
            "get": {
                "summary": "Get one Record object by Id",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The id of the record we need",
                        "required": true,
                        "type": "integer"
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "description": "Record object that need to be added",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Record"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Returns a journalist",
                        "schema": {
                            "$ref": "#/definitions/Record"
                        }
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    },
                    "404": {
                        "description": "Not found exception",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            },
            "put": {
                "summary": "Add new Record",
                "responses": {
                    "204": {
                        "description": "-"
                    },
                    "403": {
                        "description": "You have to log into application first",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    },
                    "404": {
                        "description": "Not found exception",
                        "schema": {
                            "$ref": "#/definitions/SimpleErrorMessage"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "SimpleErrorMessage": {
            "required": [
                "message"
            ],
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        },
        "RecordTypes": {
            "properties": {
                "recordTypes": {
                    "description": "Array of [RecordType](#/definitions/RecordType)",
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/RecordType"
                    }
                }
            }
        },
        "User": {
            "required": [
                "email",
                "firstName",
                "lastName",
                "password"
            ],
            "properties": {
                "firstName": {
                    "description": "The User first name",
                    "type": "string"
                },
                "lastName": {
                    "description": "The User last name",
                    "type": "string"
                },
                "country": {
                    "description": "The User country",
                    "type": "string"
                },
                "currency": {
                    "description": "The User default currency",
                    "type": "string"
                }
            }
        },
        "Pagination": {
            "properties": {
                "currentPage": {
                    "description": "The selected page",
                    "type": "integer"
                },
                "perPage": {
                    "description": "How many records per page",
                    "type": "integer"
                },
                "totalCount": {
                    "description": "How many records are in Database",
                    "type": "integer"
                },
                "prevPage": {
                    "description": "A boolean showing if there is any previous page availavble for current Pagination",
                    "type": "boolean"
                },
                "nextPage": {
                    "description": "A boolean showing if there is any next page availavble for current Pagination",
                    "type": "boolean"
                },
                "lastPage": {
                    "description": "The last available page for current Pagination",
                    "type": "integer"
                }
            }
        },
        "Record": {
            "required": [
                "name",
                "amount"
            ],
            "properties": {
                "id": {
                    "description": "The unique identifier representing a specific record.",
                    "type": "integer"
                },
                "type": {
                    "description": "The type of the record. Default value: 0",
                    "$ref": "#/definitions/RecordType"
                },
                "name": {
                    "description": "The name of the Record.",
                    "type": "string"
                },
                "amount": {
                    "description": "The amount of money spent or received.",
                    "type": "number",
                    "format": "float"
                },
                "currency": {
                    "description": "The currency of money spent or received.",
                    "type": "string"
                },
                "createdAt": {
                    "description": "The date when the record was created.",
                    "type": "string",
                    "format": "date-time"
                },
                "category": {
                    "description": "The category of the record. Default value: 1",
                    "$ref": "#/definitions/Category"
                }
            }
        },
        "Category": {
            "required": [
                "name",
                "amount"
            ],
            "properties": {
                "id": {
                    "description": "The unique identifier representing a specific record.",
                    "type": "integer"
                },
                "name": {
                    "description": "The name of category.",
                    "type": "string"
                }
            }
        },
        "QuickReport": {
            "properties": {
                "today": {
                    "description": "The amount spent today",
                    "type": "number",
                    "format": "float"
                },
                "yesterday": {
                    "description": "The amount spent yesterday",
                    "type": "number",
                    "format": "float"
                },
                "thisMonth": {
                    "description": "The amount spent this month",
                    "type": "number",
                    "format": "float"
                },
                "lastMonth": {
                    "description": "The amount spent last month",
                    "type": "number",
                    "format": "float"
                }
            }
        },
        "Categories": {
            "properties": {
                "categories": {
                    "description": "Array of [Category](#/definitions/Category)",
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/Category"
                    }
                }
            }
        },
        "Records": {
            "properties": {
                "records": {
                    "description": "Array of [Record](#/definitions/Record)",
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/Record"
                    }
                },
                "pagination": {
                    "description": "Pagination object",
                    "$ref": "#/definitions/Pagination"
                }
            }
        },
        "RecordType": {
            "required": [
                "id"
            ],
            "properties": {
                "id": {
                    "description": "Unique identifier representing a specific record type.",
                    "type": "integer"
                },
                "name": {
                    "description": "The name of the RecordType.",
                    "type": "string"
                }
            }
        }
    }
}