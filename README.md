Application
===========

vagrant up
vagrant ssh
cd /var/www/spendings/application
composer install
php app/console doctrine:database:create
php app/console doctrine:schema:create
php app/console d:m:m
