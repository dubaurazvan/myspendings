# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

    config.vm.box = "scotch/box"
    config.vm.network "private_network", ip: "192.168.99.111"
    config.vm.hostname = "spendings"
    config.vm.synced_folder ".", "/var/www/spendings", :mount_options => ["dmode=777", "fmode=666"]

    # Optional NFS. Make sure to remove other synced_folder line too
    # config.vm.synced_folder ".", "/var/www/spendings", :nfs => { :mount_options => ["dmode=777","fmode=666"] }

    config.vm.provision "shell", path: "shell/nginx.sh"

    config.vm.provision "shell", inline: <<-SHELL
        # disable apache
        service apache2 stop

        apt-get install -q -y php5-dev

        # configure fpm
        cp -R /var/www/spendings/shell/files/php5/* /etc/php5
        service php5-fpm restart
    SHELL

    config.vm.provision "shell", inline: <<-SHELL
        cp /var/www/spendings/shell/files/nginx/sites-enabled/spendings.conf /etc/nginx/sites-enabled/spendings.conf
        service nginx restart
    SHELL

    config.vm.provision "shell", inline: <<-SHELL
        cd /var/www/spendings

        composer self-update

        if [ ! -d "vendor/twig/" ]; then
            composer install

            cd vendor/twig/twig/ext/twig
            phpize
            ./configure
            make
            make install

            echo "extension=twig.so" > /etc/php5/mods-available/twig.ini
            service nginx restart

            wget --no-check-certificate https://github.com/aglover/ubuntu-equip/raw/master/equip_java7_64.sh && bash equip_java7_64.sh

            sudo rm /etc/nginx/sites-enabled/default.conf
        fi
    SHELL
end
